# Dotfiles

I use [GNU/stow](https://www.gnu.org/software/stow/) to manage my dotfiles.

To use it, clone this repo in `~/.dotfiles` with:

```console
git clone https://gitlab.com/dimitrissp/dotfiles.git ~/.dotfiles
```

They can be installed by calling `stow <folder>` for particular packages or just `make install` to install everything.

Additional dotfiles outside this repo:

* [XMonad](https://gitlab.com/dimitrissp/xmonad-configuration)
* [Emacs](https://gitlab.com/dimitrissp/emacs-configuration)


